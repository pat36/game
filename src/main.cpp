#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Audio/Music.hpp>
#include <STP/TMXLoader.hpp>
#define SCREEN_HEIGHT 1080
#define SCREEN_WIDTH 1920
int main(){
	//ustawienia okna i wyłączenie kursora
	sf::RenderWindow window(
            sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32), 
            "Gra", 
            sf::Style::Fullscreen
            );

	window.sf::RenderWindow::setMouseCursorVisible(false);
	//odtwarzanie muzyki
	sf::Music theme;
	if(!theme.openFromFile("music/475943_Skyrim-8-Bit-Theme.ogg"))
			return EXIT_FAILURE;
	theme.setLoop(true);
	theme.play();
	//załadowanie mapy
	tmx::TileMap map("maps/map0.tmx");
	//tekstura postaci, sprite postaci i jej ustawienia
	sf::Texture mc_up;
	if(!mc_up.loadFromFile("graphics/chars/mc/mc_up.png"))
		return EXIT_FAILURE;
	sf::Texture mc_down;
	if(!mc_down.loadFromFile("graphics/chars/mc/mc_down.png"))
		return EXIT_FAILURE;
	
	sf::Texture mc_left;
	if(!mc_left.loadFromFile("graphics/chars/mc/mc_left.png"))
		return EXIT_FAILURE;
	sf::Texture mc_right;
	if(!mc_right.loadFromFile("graphics/chars/mc/mc_right.png"))
		return EXIT_FAILURE;

	sf::Sprite player(mc_down);
	player.sf::Transformable::setScale(1.5, 1.5);
	player.sf::Transformable::setPosition(640, 310);
	
	while(window.isOpen()){
		
		//sterowanie
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			window.close();
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
			player.sf::Sprite::setTexture(mc_right);
			player.sf::Transformable::move(1.0, 0.0);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
			player.sf::Sprite::setTexture(mc_left);
			player.sf::Transformable::move(-1.0, 0.0);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
			player.sf::Sprite::setTexture(mc_up);
			player.sf::Transformable::move(0.0, -1.0);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
			player.sf::Sprite::setTexture(mc_down);
			player.sf::Transformable::move(0.0, 1.0);
		}
		
		//sterowanie wyświetlaniem
		window.clear(sf::Color::Black);
		window.draw(map);
		window.draw(player);
		window.display();
	}

	return 0;
}
